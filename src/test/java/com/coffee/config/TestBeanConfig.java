package com.coffee.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "com.coffee")
public class TestBeanConfig {

}
