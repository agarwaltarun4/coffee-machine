package com.coffee;

import com.coffee.config.TestBeanConfig;
import com.coffee.dto.Beverage;
import com.coffee.dto.CoffeeMachine;
import com.coffee.dto.Item;
import com.coffee.service.CoffeeMachineServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestBeanConfig.class})
public class CoffeeControllerTest {

    @Autowired
    private CoffeeMachineServiceImpl coffeeMachineService;

    @Test
    public void test1WithoutThreads() throws JSONException, JsonProcessingException {

        System.out.println("================test1WithoutThreads================");
        CoffeeMachine coffeeMachine = getCoffeeMachine();

        System.out.println("hot_tea is preparing");
        Assert.assertEquals("hot_tea is prepared", coffeeMachineService.dispense(coffeeMachine.getBeverageList().get("hot_tea")));
        System.out.println("hot_tea is prepared");

        System.out.println("hot_coffee is preparing");
        Assert.assertEquals("hot_coffee is prepared", coffeeMachineService.dispense(coffeeMachine.getBeverageList().get("hot_coffee")));
        System.out.println("hot_coffee is prepared");

        System.out.println("green_tea is preparing");
        Assert.assertEquals("green_tea cannot be prepared because green_mixture is not available",
                coffeeMachineService.dispense(coffeeMachine.getBeverageList().get("green_tea")));
        System.out.println("green_tea cannot be prepared because green_mixture is not available");

        System.out.println("Refilling sugar_syrup");
        coffeeMachineService.refill(CoffeeMachine.getItems().get("sugar_syrup"));
        Assert.assertEquals(CoffeeMachine.getItems().get("sugar_syrup").getCapacity(), CoffeeMachine.getItems().get("sugar_syrup").getQty());
        System.out.println("Refilling sugar_syrup successfull");

        System.out.println("black_tea is preparing");
        Assert.assertEquals("black_tea cannot be prepared because item hot_water is not sufficient",
                coffeeMachineService.dispense(coffeeMachine.getBeverageList().get("black_tea")));
        System.out.println("black_tea cannot be prepared because item hot_water is not sufficient");
        System.out.println("================END=======================");
    }

    @Test
    public void test2WithoutThreads() throws JSONException, JsonProcessingException {

        System.out.println("================test2WithoutThreads================");

        CoffeeMachine coffeeMachine = getCoffeeMachine();

        System.out.println("hot_tea is preparing");
        Assert.assertEquals("hot_tea is prepared", coffeeMachineService.dispense(coffeeMachine.getBeverageList().get("hot_tea")));
        System.out.println("hot_tea is prepared");

        System.out.println("hot_coffee is preparing");
        Assert.assertEquals("hot_coffee is prepared", coffeeMachineService.dispense(coffeeMachine.getBeverageList().get("hot_coffee")));
        System.out.println("hot_coffee is prepared");

        System.out.println("green_tea is preparing");
        Assert.assertEquals("green_tea cannot be prepared because green_mixture is not available",
                coffeeMachineService.dispense(coffeeMachine.getBeverageList().get("green_tea")));
        System.out.println("green_tea cannot be prepared because green_mixture is not available");

        System.out.println("Refilling sugar_syrup");
        coffeeMachineService.refill(CoffeeMachine.getItems().get("sugar_syrup"));
        Assert.assertEquals(CoffeeMachine.getItems().get("sugar_syrup").getCapacity(), CoffeeMachine.getItems().get("sugar_syrup").getQty());
        System.out.println("Refilled sugar_syrup successfully");

        System.out.println("Refilling hot_water");
        coffeeMachineService.refill(CoffeeMachine.getItems().get("hot_water"));
        Assert.assertEquals(CoffeeMachine.getItems().get("hot_water").getCapacity(), CoffeeMachine.getItems().get("hot_water").getQty());
        System.out.println("Refilled hot_water successfully");

        System.out.println("black_tea is preparing");
        Assert.assertEquals("black_tea is prepared",
                coffeeMachineService.dispense(coffeeMachine.getBeverageList().get("black_tea")));
        System.out.println("black_tea is prepared");

        System.out.println("================END=======================");
    }

    @Test
    public void test3WithThreads() throws JSONException, JsonProcessingException {

        System.out.println("================test3WithThreads================");
        final CoffeeMachine coffeeMachine = getCoffeeMachine();

        List<String> beverages = new ArrayList<>();
        beverages.add("hot_tea");
        beverages.add("hot_coffee");
        beverages.add("green_tea");
        beverages.add("black_tea");
        ExecutorService e = Executors.newFixedThreadPool(coffeeMachine.getNoOfOutlets().intValue());

        for (final String beverage : beverages) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println(beverage + " is preparing");
                        String s = coffeeMachineService.dispense(coffeeMachine.getBeverageList().get(beverage));
                        System.out.println(s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            e.submit(r);
        }
        e.shutdown();
        try {
            e.awaitTermination(1000, TimeUnit.SECONDS);
        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }

        System.out.println("================END=======================");
    }

    @Test
    public void test4WithThreads() throws JSONException, JsonProcessingException {

        System.out.println("================test4WithThreads================");
        final CoffeeMachine coffeeMachine = getCoffeeMachine();

        List<String> beverages = new ArrayList<>();

        beverages.add("hot_tea");
        beverages.add("hot_coffee");
        beverages.add("green_tea");
        beverages.add("black_tea");


        ExecutorService e = Executors.newFixedThreadPool(coffeeMachine.getNoOfOutlets().intValue());


        for (final String beverage : beverages) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println(beverage + " is preparing");
                        String s = coffeeMachineService.dispense(coffeeMachine.getBeverageList().get(beverage));
                        System.out.println(s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            e.submit(r);
        }
        e.shutdown();
        try {
            e.awaitTermination(1000, TimeUnit.SECONDS);
        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }

        final List<String> refillItems = new ArrayList<>();
        refillItems.add("sugar_syrup");
        refillItems.add("hot_water");
        refillItems.add("hot_milk");

        e = Executors.newFixedThreadPool(coffeeMachine.getNoOfOutlets().intValue());
        for (final String refillItem : refillItems) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println("Refilling " + refillItem);
                        coffeeMachineService.refill(CoffeeMachine.getItems().get(refillItem));
                        System.out.println("Refilled " + refillItem + " Successfully");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            e.submit(r);
        }
        e.shutdown();
        try {
            e.awaitTermination(1000, TimeUnit.SECONDS);
        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }

        e = Executors.newFixedThreadPool(coffeeMachine.getNoOfOutlets().intValue());
        for (final String beverage : beverages) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println(beverage + " is preparing");
                        String s = coffeeMachineService.dispense(coffeeMachine.getBeverageList().get(beverage));
                        System.out.println(s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            e.submit(r);
        }
        e.shutdown();
        try {
            e.awaitTermination(1000, TimeUnit.SECONDS);
        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }

        System.out.println("================END=======================");
    }

    //    public
    private CoffeeMachine getCoffeeMachine() throws JSONException, JsonProcessingException {
        JSONObject jsonObject = new JSONObject("{\"machine\":{\"outlets\":{\"count_n\":3},\"beverages\":{\"hot_tea\":{\"hot_milk\":100,\"hot_water\":200,\"sugar_syrup\":10,\"ginger_syrup\":10,\"tea_leaves_syrup\":30},\"black_tea\":{\"hot_water\":300,\"sugar_syrup\":50,\"ginger_syrup\":30,\"tea_leaves_syrup\":30},\"green_tea\":{\"hot_water\":100,\"sugar_syrup\":50,\"ginger_syrup\":30,\"green_mixture\":30},\"hot_coffee\":{\"hot_milk\":400,\"hot_water\":100,\"sugar_syrup\":50,\"ginger_syrup\":30,\"tea_leaves_syrup\":30}},\"total_items_quantity\":{\"hot_milk\":500,\"hot_water\":500,\"sugar_syrup\":100,\"ginger_syrup\":100,\"tea_leaves_syrup\":100}}}");
        JSONObject machine = jsonObject.getJSONObject("machine");
        Long outletsCnt = machine.getJSONObject("outlets").getLong("count_n");
        JSONObject totalItemsQty = machine.getJSONObject("total_items_quantity");
        JSONObject beverages = machine.getJSONObject("beverages");

        Map<String, Integer> items = new ObjectMapper().readValue(totalItemsQty.toString(), HashMap.class);
        Map<String, HashMap<String, Integer>> beverageObjects = new ObjectMapper().readValue(beverages.toString(), HashMap.class);

        for (Map.Entry<String, Integer> entry : items.entrySet()) {
            Item item = new Item(entry.getKey(), entry.getValue().longValue());
            CoffeeMachine.getItems().put(entry.getKey(), item);
        }

        HashMap<String, Beverage> beverageList = new HashMap<>();
        for (Map.Entry<String, HashMap<String, Integer>> entry : beverageObjects.entrySet()) {

            List<Item> recipeList = new ArrayList<>();
            for (Map.Entry<String, Integer> recipe : entry.getValue().entrySet()) {
                Item item = new Item(recipe.getKey(), recipe.getValue().longValue());
                recipeList.add(item);
            }
            beverageList.put(entry.getKey(), new Beverage(entry.getKey(), recipeList));
        }

        return new CoffeeMachine(outletsCnt, beverageList);
    }

}

