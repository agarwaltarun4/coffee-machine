package com.coffee.service;

import com.coffee.dto.Beverage;

public interface CoffeeMachineService {
    public String dispense(Beverage beverage);
}
