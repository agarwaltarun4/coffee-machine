package com.coffee.service;

import com.coffee.dto.Item;

public interface ItemService {
    public void refill(Item item);
}
