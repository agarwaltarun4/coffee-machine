package com.coffee.service;

import com.coffee.dto.Item;
import org.springframework.stereotype.Service;

@Service("itemService")
public class ItemsServiceImpl implements ItemService {

    @Override
    public void refill(Item item) {
        item.setQty(item.getCapacity());
    }
}
