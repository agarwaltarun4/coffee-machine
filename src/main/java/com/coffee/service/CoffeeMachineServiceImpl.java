package com.coffee.service;

import com.coffee.dto.Beverage;
import com.coffee.dto.CoffeeMachine;
import com.coffee.dto.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("coffeeMachineService")
public class CoffeeMachineServiceImpl implements CoffeeMachineService {

    @Autowired
    private final ItemsServiceImpl itemsService;

    public CoffeeMachineServiceImpl(ItemsServiceImpl itemsService) {
        this.itemsService = itemsService;
    }

    @Override
    public String dispense(Beverage beverage) {

        List<Item> recipe = beverage.getRecipe();

        boolean isAllItemsAvailable = checkIfAllItemsAvailable(recipe);

        if(!isAllItemsAvailable){
            return beverage.getName() + " cannot be prepared because " + getItemNotAvailable(recipe) + " is not available";
        }

        synchronized (this) {
            if (checkIfAllItemsHaveSufficientQty(recipe)) {
                updateItemsCapacity(recipe);
            } else {
                return beverage.getName() + " cannot be prepared because item " + getItemNotHaveSufficientQty(recipe) + " is not sufficient";
            }
        }
        return beverage.getName()+" is prepared";
    }

    public synchronized void refill(Item item){
        itemsService.refill(item);
    }

    private void updateItemsCapacity(List<Item> recipe) {
        for (Item item:recipe) {
            Long qtyAvailable = CoffeeMachine.getItems().get(item.getName()).getQty();
            CoffeeMachine.getItems().get(item.getName()).setQty(qtyAvailable-item.getQty());
        }
    }

    private String getItemNotHaveSufficientQty(List<Item> recipe) {
        for (Item item:recipe) {
            if(!CoffeeMachine.getItems().containsKey(item.getName()) ||
                    !(CoffeeMachine.getItems().get(item.getName()).getQty()>=item.getQty())){
                return item.getName();
            }
        }
        return "";
    }

    private String getItemNotAvailable(List<Item> recipe) {
        for (Item item:recipe) {
            if(!CoffeeMachine.getItems().containsKey(item.getName())){
                return item.getName();
            }
        }
        return "";
    }

    private boolean checkIfAllItemsAvailable(List<Item> recipe){
        for (Item item:recipe) {
            if(!CoffeeMachine.getItems().containsKey(item.getName())){
                return false;
            }
        }
        return true;
    }

    private boolean checkIfAllItemsHaveSufficientQty(List<Item> recipe) {
        for (Item item:recipe) {
            if(!CoffeeMachine.getItems().containsKey(item.getName()) ||
                    !(CoffeeMachine.getItems().get(item.getName()).getQty()>=item.getQty())){
                return false;
            }
        }
        return true;
    }
}
