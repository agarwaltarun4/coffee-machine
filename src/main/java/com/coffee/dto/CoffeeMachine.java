package com.coffee.dto;

import java.util.HashMap;

public class CoffeeMachine {
    private Long noOfOutlets;
    private static HashMap<String,Item> items = new HashMap<>();
    private HashMap<String,Beverage> beverageList;

    public CoffeeMachine() {
    }

    public CoffeeMachine(Long noOfOutlets, HashMap<String, Beverage> beverageList) {
        this.noOfOutlets = noOfOutlets;
        this.beverageList = beverageList;
    }

    public Long getNoOfOutlets() {
        return noOfOutlets;
    }

    public void setNoOfOutlets(Long noOfOutlets) {
        this.noOfOutlets = noOfOutlets;
    }

    public static HashMap<String, Item> getItems() {
        return items;
    }

    public static void setItems(HashMap<String, Item> items) {
        CoffeeMachine.items = items;
    }

    public HashMap<String, Beverage> getBeverageList() {
        return beverageList;
    }

    public void setBeverageList(HashMap<String, Beverage> beverageList) {
        this.beverageList = beverageList;
    }
}
