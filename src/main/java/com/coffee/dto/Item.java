package com.coffee.dto;

import java.util.HashMap;

public class Item {
    private String name;
    private Long qty;
    private Long capacity;

    public Item() {
    }

    public Item(String name, Long capacity) {
        this.name = name;
        this.qty = capacity;
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }
}
