package com.coffee.dto;

import java.util.List;

public class Beverage {
    private String name;
    private List<Item> recipe;

    public Beverage() {
    }

    public Beverage(String name, List<Item> recipe) {
        this.name = name;
        this.recipe = recipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Item> getRecipe() {
        return recipe;
    }

    public void setRecipe(List<Item> recipe) {
        this.recipe = recipe;
    }
}
